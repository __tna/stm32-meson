# Show commands as they are run.
set -x
# Exit on error.
set -e
ROOT=$(dirname $(realpath $0))/..

chips=('STM32G081GB' 'STM32G081KB' 'STM32G081CB' 'STM32G081RB' 'STM32G081EB' 'STM32G071GB' 'STM32G071KB' 'STM32G071CB' 'STM32G071RB' 'STM32G071EB' 'STM32G071G8' 'STM32G071K8' 'STM32G071C8' 'STM32G071R8' 'STM32G041F8' 'STM32G041G8' 'STM32G041K8' 'STM32G041C8' 'STM32G041Y8' 'STM32G031F8' 'STM32G031G8' 'STM32G031K8' 'STM32G031C8' 'STM32G031Y8' 'STM32G041J6' 'STM32G041F6' 'STM32G041G6' 'STM32G041K6' 'STM32G041C6' 'STM32G031J6' 'STM32G031F6' 'STM32G031G6' 'STM32G031K6' 'STM32G031C6' 'STM32G031J4' 'STM32G031F4' 'STM32G031G4' 'STM32G031K4' 'STM32G031C4')


for c in ${chips[@]}
do
    echo Testing $c...
    builddir=$ROOT/build/chip-$c
    rm -rf $builddir
    mkdir -p $builddir
    meson $builddir --cross-file stm32-meson/stm32g0.build -Dstm32_chip=$c -Dstm32_cube_dir=$stm32_cube_dir
    ninja -C $builddir
done
